package net.sansa_stack.template.spark.rdf.util


import java.util

import net.sansa_stack.template.spark.rdf.models.{JoinableRows, TableRow}
import opennlp.tools.stemmer.PorterStemmer
import org.apache.commons.lang3.StringUtils

import scala.collection.JavaConverters._
import scala.collection.mutable

object NoSQLJoinPredictor extends App {

  def inferJoins(tableRows: Array[TableRow]): List[JoinableRows] = {
    val stack = mutable.Stack[TableRow]().pushAll(tableRows)
    val arrayList = new util.ArrayList[JoinableRows]()

    while (stack.nonEmpty) {
      val currentRow = stack.pop()
      stack.foreach(targetRow => {
        val maybeRows = compareColumns(currentRow, targetRow)
        if (maybeRows.isDefined) arrayList.add(maybeRows.get)
      })
    }
    arrayList.asScala.toList
  }
  /** USE THIS Template to check the scoring logic directly */
 // println(compareColumns(TableRow("USERS", "INT", 10, "role_id"), TableRow("USER_ROLES", "INT", 10, "id")))

  private def compareColumns(currentRow: TableRow, targetRow: TableRow): Option[JoinableRows] = {

    if (currentRow.columnName.equals("id") && targetRow.columnName.equals("id")) return None
    if (currentRow.tableName.equals(targetRow.tableName)) return None

    val currentTableTokens = tokenize(currentRow.tableName)
    val currentColumnTokens = tokenize(currentRow.columnName)
    val currentColumnDataType = currentRow.columnDataType
    val currentColSize = currentRow.columnSize

    val targetTableTokens = tokenize(targetRow.tableName)
    val targetColumnTokens = tokenize(targetRow.columnName)
    val targetColumnDataType = targetRow.columnDataType
    val targetColSize = targetRow.columnSize

    if (isDataTypeCompatible(currentColumnDataType, targetColumnDataType)) {
      val probability = calculateMatch(currentTableTokens, currentColumnTokens, currentColumnDataType, currentColSize,
        targetTableTokens, targetColumnTokens, targetColumnDataType, targetColSize
      )
      return Some(JoinableRows(currentRow.tableName, currentRow.columnName, targetRow.tableName, targetRow.columnName, probability, 100))
    }
    return None
  }


  private def isDataTypeCompatible(currentColumnDataType: String, targetColumnTokens: String): Boolean = {
    currentColumnDataType.equals(targetColumnTokens)
  }


  private def calculateMatch(currentTableTokens: Set[String], currentColumnsTokens: Set[String], currentColDataType: String, currentColSize: Int,
                             targetTableTokens: Set[String], targetColumnsTokens: Set[String], targetColDataType: String, targetColSize: Int
                            ): Float = {
    var probability: Float = 0.00f
    //TODO: Using List instead of sets

    val combinedCurrentTokens = currentTableTokens ++ currentColumnsTokens
    val combinedTargetTokens = targetTableTokens ++ targetColumnsTokens
    val totalTokens = combinedCurrentTokens.union(combinedTargetTokens)
    val commonTokens = combinedCurrentTokens.intersect(combinedTargetTokens)

    // is column name matches for both table ?
    if (currentColumnsTokens.equals(targetColumnsTokens)) {
      //is matching column names has the term 'id' in it ?
      if (currentColumnsTokens.contains("id") && targetColumnsTokens.contains("id")) {
        //good chances are PK FK relation
        probability = 0.95f
      } else {
        //is the matching column names are more general ?
        //find how both table's terms coo relates
        probability = Float.box(commonTokens.size) / Float.box(totalTokens.size)

        //if column terms are present in either of the tables name, boost the probability to some extent.
        val tableNamesCombined = currentTableTokens ++ targetTableTokens
        val columnNameToBothTableNames = currentColumnsTokens.intersect(tableNamesCombined)

        if (columnNameToBothTableNames.nonEmpty) probability += (.30f + Float.box(Float.box(columnNameToBothTableNames.size) / Float.box(totalTokens.size)))
        if (probability > 1.0f) probability = 0.95f
      }
    } else if (currentColumnsTokens.contains("id") && targetColumnsTokens.contains("id")) {
      probability = Float.box(commonTokens.size) / Float.box(totalTokens.size)
    }

    return probability
  }

  private def tokenize(name: String): Set[String] = {
    var tokens: List[String] = List()
    val lowerCased = name.toLowerCase
    val stemmer = new PorterStemmer()

    if (name.contains("_")) {
      tokens = tokens ++ StringUtils.splitPreserveAllTokens(lowerCased, "_").toList
    } else if (name.contains("-")) {
      tokens = tokens ++ StringUtils.splitPreserveAllTokens(lowerCased, "-").toList
    } else if (name.contains(" ")) {
      tokens = tokens ++ StringUtils.splitPreserveAllTokens(lowerCased, " ").toList
    } else {
      tokens = tokens ++ StringUtils.splitByCharacterTypeCamelCase(name).toList
    }
    tokens.map(stemmer.stem).toSet
  }
}
