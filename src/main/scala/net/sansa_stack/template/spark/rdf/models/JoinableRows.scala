package net.sansa_stack.template.spark.rdf.models

case class JoinableRows(tableName:String,
                        columnName:String,
                        otherTableName:String,
                        OtherColumnName:String,
                        confidence : Float,
                        relevance : Float
                        )
{
  override def toString: String = s"$tableName.$columnName can be joined with $otherTableName.$OtherColumnName with ${confidence *100}% confidence"
}
