package net.sansa_stack.template.spark.rdf

import javax.sql.rowset.Joinable
import org.apache.jena.riot.Lang
import org.apache.spark.sql.{Row, SparkSession}
import net.sansa_stack.rdf.spark.io._
import net.sansa_stack.query.spark.query._
import net.sansa_stack.template.spark.rdf.models.{JoinableRows, TableRow}
import net.sansa_stack.template.spark.rdf.util.NoSQLJoinPredictor
import org.apache.commons.lang3.StringUtils
import org.apache.jena.graph.Triple
import org.apache.spark.rdd.RDD

import scala.collection.JavaConverters._


object TripleReader {

  def main(args: Array[String]) {


    val spark = SparkSession.builder
      .appName(s"Triple reader example")
      .master("local[*]")
      .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .getOrCreate()

    val lang = Lang.NTRIPLES

    val triples = spark.rdf(lang)("/home/shredder/Desktop/rdf_oracle.nt") //Sample nt files present at res folder

    //Need to fix issue where DF col name changes with number of tables supplied
    // https://github.com/SANSA-Stack/SANSA-Query/issues/47#issuecomment-639755055

/*    getNonSQLJoins(triples,
      List("RDSORACLEFORPRESTO.BANK",
        "RDSORACLEFORPRESTO.ACQUISITIONS",
        "RDSORACLEFORPRESTO.CARD_CENTRES",
        "RDSORACLEFORPRESTO.TRANSACTION_DATA",
        "RDSORACLEFORPRESTO.GEOGRAPHIC_DIMENSION",
        "RDSORACLEFORPRESTO.PHARMA_SALES_FACT"
      )
    )*/ // For rdf_oracle.nt

    //getRelationalJoins(triples,"FRED.FRED.CIVILIAN_LABOR_FORCE"); //for rdf_fred.nt
    getRelationalJoins(triples,"RDSORACLEFORPRESTO.US_REGIONS"); //for rdf_oracle.nt

    spark.stop
  }

  def getNonSQLJoins(triples: RDD[Triple], tableNames: List[String]): Unit = {

    val str = tableNames.map(x => s"""strends(str(?col), 'table/$x')""").reduceLeft((x, y) => s"$x || $y")
    val finalString = "FILTER(".concat(str).concat(")")

    val query =
      s"""    PREFIX ns0: <https://www.pm61data.com/discovery/catalog/>
                        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT ?tableName ?columnName ?dataType
                        WHERE
                        {
                            {
                              SELECT ?ColumnRef
                              WHERE
                                {
                                   ?col ns0:column ?ColumnRef .
                                    $finalString
                                }
                            }

                            ?objectsContainingColObj ns0:column ?ColumnRef .
                            ?objectsContainingColObj ns0:tableType "table" .
                            ?objectsContainingColObj ns0:name ?tableName .

                            ?ColumnRef ns0:columnDataType ?colDataTypeRef .
                            ?ColumnRef ns0:name ?columnName .
                            ?colDataTypeRef ns0:fullName ?dataType .
                        }
                    """

    val result = triples.sparql(query)
    val rows = result.collect()
    val tableRowsArray: Array[TableRow] = rows.map(row => TableRow(row.getString(1), row.getString(2), 0, row.getString(4))) //Need to fix issue where DF col name changes with number of tables supplied

    val probableJoinsList = NoSQLJoinPredictor.inferJoins(tableRowsArray)
    probableJoinsList.filter(x => x.confidence != 0f).foreach(println)
    probableJoinsList.foreach(println)

    result.show()
  }


  def getRelationalJoins(triples: RDD[Triple], tableName: String): Unit = {
    val query =
      s""" PREFIX ns0: <https://www.pm61data.com/discovery/catalog/>
                         PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

                        SELECT ?pkTableName ?pkColName ?fkTableName ?fkColName
                        WHERE
                        {
                            {
                              SELECT ?value
                              WHERE
                                {
                                   ?col ns0:foreignKey ?value .
                                    FILTER(strends(str(?col), 'table/$tableName'))
                                }
                            }

                           ?value ns0:columnReference ?colRef .
                           ?colRef ns0:foreignKeyColumn ?fkColObj .
                           ?colRef ns0:primaryKeyColumn ?pkColObj .

                           ?fkColObj ns0:name ?fkColName .
                           ?objectsContainingFkColObj ns0:column ?fkColObj .
                           ?objectsContainingFkColObj ns0:tableType "table" .
                           ?objectsContainingFkColObj ns0:name ?fkTableName .

                           ?pkColObj ns0:name ?pkColName .
                           ?objectsContainingPkColObj ns0:column ?pkColObj .
                           ?objectsContainingPkColObj ns0:tableType "table" .
                           ?objectsContainingPkColObj ns0:name ?pkTableName .
                        }
                    """

    val result = triples.sparql(query)
    val rows = result.collect()

    result.show()

    println(s" FOR Table -> ${tableName} , Possible joins are: ")
    val joinablesList = rows.map(row => JoinableRows(row.getString(4), row.getString(7), row.getString(6), row.getString(0), 1.00f, 100))

    joinablesList.foreach(println)
  }

}