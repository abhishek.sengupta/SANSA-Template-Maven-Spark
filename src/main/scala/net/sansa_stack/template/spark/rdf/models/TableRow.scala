package net.sansa_stack.template.spark.rdf.models

case class TableRow(tableName:String,
                    columnDataType:String,
                    columnSize:Int,
                    columnName:String
                   )
